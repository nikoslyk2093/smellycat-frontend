export interface EmailPayload {
    recipients: string[];
    cc: string[];
    bcc: string[];
    message: string;
    subject: string;
    priority: number;
    replyTo: string;
}