import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-contact-form-success',
  templateUrl: './contact-form-success.component.html',
  styleUrls: ['./contact-form-success.component.css']
})
export class ContactFormSuccessComponent {
  constructor(public activeModal: NgbActiveModal) {}
}
