import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MailService } from 'src/app/services/mailService';
import { ContactFormSuccessComponent } from '../modals/contact-form-success/contact-form-success.component';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent {
  constructor(private emailService: MailService, private modalService: NgbModal) { }
  submitted: boolean = false;

  contactForm = new FormGroup({
    fullName: new FormControl('', { nonNullable: true, validators: [Validators.required] }),
    email: new FormControl('', { nonNullable: true, validators: [Validators.required, Validators.email] }),
    city: new FormControl('', { nonNullable: true, validators: [Validators.required] }),
    postalCode: new FormControl('', { nonNullable: true, validators: [Validators.required] }),
    address: new FormControl('', { nonNullable: true, validators: [Validators.required] }),
    message: new FormControl(null),
    tos: new FormControl(false, { nonNullable: true, validators: [Validators.requiredTrue] })
  });
  
  sendEmail() {
    this.submitted = true;

    if (this.contactForm.invalid) {
      return;
    }

    const success = this.emailService.sendContactForm(this.contactForm.getRawValue());

    if (success) {
      this.modalService.open(ContactFormSuccessComponent, { centered: true });

      this.submitted = false;
      this.contactForm.reset();
    }
  }
}
