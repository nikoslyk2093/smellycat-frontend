import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ContactDetails } from "../models/contact-details";
import { environment } from "../environments/environment";
import { EmailPayload } from "../models/emailPayload";

@Injectable({
    providedIn: 'root',
})
export class MailService {
    constructor(private http: HttpClient) { }

    headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${environment.mailApiKey}`,
    });

    sendContactForm(contactDetails: ContactDetails) : boolean {
        if(!environment.production) { return true; } // mock successful send of email
        let success: boolean = false;

        const emailPayload = this.getPayloadFromContactForm(contactDetails);

        this.http.post(environment.mailApiEndpoint, emailPayload, { observe: 'response' })
        .subscribe(res => {
            success = res.status == 200
        });

        return success;
    }

    private getPayloadFromContactForm(contactDetails: ContactDetails): EmailPayload {

        const payload: EmailPayload = {
            replyTo: contactDetails.email,
            message: contactDetails.message ?? '',
            recipients: [ environment.companyEmail ],
            priority: 1,
            subject: 'Submitted Contact Form From ' + contactDetails.fullName,
            cc: [],
            bcc: []
        };

        return payload;
    }
}