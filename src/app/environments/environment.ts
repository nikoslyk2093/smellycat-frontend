export const environment = {
    production: false,
    mailApiEndpoint: "YOUR_API_URL",
    mailApiKey: 'ADD_YOUR_KEY',
    companyEmail: 'YOUR_EMAIL_HERE'
}